use itertools::Itertools;
use std::fmt::Debug;

/// The newline character.
const NEWLINE: char = '\n';

/// The properly escaped string literal "\n" for printing out newline bytes.
const NEWLINE_STR: &'static str = "\\n";

#[derive(Debug)]
pub struct Token {
  pub line: u32,
  pub column: u32,
  pub text: String,
}

#[derive(Debug)]
pub enum ParserError {
  UnterminatedString,
}

enum ParserAction {
  Halt,
  Advance,
}

pub struct Parser {
  pub chars: Vec<char>,
  pub line_mask: Vec<u32>,
  pub column_mask: Vec<u32>,
  pub next_non_whitespace: Vec<u32>,
  pub cursor: u32,
}

impl Debug for Parser {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    writeln!(f, "BetterParser {{")?;
    writeln!(f, "  cursor: {}", self.cursor)?;
    writeln!(f, "  content: {{")?;
    for i in 0..self.chars.len() {
      writeln!(
        f,
        "{i:3}    {:>3} {} {} {}",
        if self.chars[i] == NEWLINE {
          NEWLINE_STR.to_string()
        } else {
          self.chars[i].to_string()
        },
        self.line_mask[i],
        self.column_mask[i],
        self.next_non_whitespace[i]
      )?;
    }
    writeln!(f, "  }}")?;
    writeln!(f, "}}")
  }
}

impl Parser {
  pub fn new(input: String) -> Parser {
    let chars = input.chars().collect_vec();

    // Initialize positional masks
    let mut current_line = 1;
    let mut current_col = 1;
    let mut line_mask = vec![0u32; chars.len()];
    let mut column_mask = vec![0u32; chars.len()];
    chars
      .iter()
      .zip(line_mask.iter_mut().zip(column_mask.iter_mut()))
      .for_each(|(&char, (line, col))| {
        *line = current_line;
        *col = current_col;
        if char == NEWLINE {
          current_line += 1;
          current_col = 0;
        }
        current_col += 1;
      });

    // Initialize whitespace jump table
    let mut last_non_whitespace = chars.len();
    let mut next_non_whitespace = vec![0u32; chars.len()];
    chars
      .iter()
      .enumerate()
      .rev()
      .zip(next_non_whitespace.iter_mut().rev())
      .for_each(|((i, char), jump)| {
        if !char.is_whitespace() {
          last_non_whitespace = i;
        }
        *jump = last_non_whitespace as u32;
      });

    Parser {
      chars,
      line_mask,
      column_mask,
      next_non_whitespace,
      cursor: 0,
    }
  }

  #[inline]
  fn at_eoi(&self) -> bool {
    self.cursor == self.chars.len() as u32
  }

  #[inline]
  fn skip_whitespace(&mut self) {
    if !self.at_eoi() {
      self.cursor = self.next_non_whitespace[self.cursor as usize];
    }
  }

  #[inline]
  #[rustfmt::skip]
  fn next_step(&self, in_string: bool) -> Result<ParserAction, ParserError> {
    use ParserAction::*;
    use ParserError::*;
    let at_eoi = self.at_eoi();
    let cursor = self.cursor as usize;
    let is_whitespace = !at_eoi && self.chars[cursor].is_whitespace();
    let prev_is_quote = cursor != 0 && self.chars[cursor - 1] == '"';
    match (at_eoi, in_string, is_whitespace, prev_is_quote) {
      (      true,      true,             _,             _) => Err(UnterminatedString),
      (      true,     false,             _,             _) |
      (         _,      true,          true,          true) |
      (         _,     false,          true,             _) => Ok(Halt),
      _                                                     => Ok(Advance),
    }
  }

  pub fn consume_word(&mut self) -> Result<Option<Token>, ParserError> {
    self.skip_whitespace();

    let start_idx = self.cursor as usize;
    let in_string = !self.at_eoi() && self.chars[start_idx] == '"';
    loop {
      match self.next_step(in_string)? {
        ParserAction::Halt => break,
        ParserAction::Advance => self.cursor += 1,
      }
    }

    let cursor = self.cursor as usize;
    Ok((start_idx != cursor).then(|| Token {
      line: self.line_mask[start_idx],
      column: self.column_mask[start_idx],
      text: self.chars[start_idx..cursor].iter().collect(),
    }))
  }
}
